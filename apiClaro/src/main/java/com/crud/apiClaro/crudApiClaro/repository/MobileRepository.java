package com.crud.apiClaro.crudApiClaro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.crud.apiClaro.crudApiClaro.model.Mobile;

public interface MobileRepository extends JpaRepository<Mobile, Long> {

}
