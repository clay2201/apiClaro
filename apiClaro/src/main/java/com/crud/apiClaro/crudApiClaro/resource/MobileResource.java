package com.crud.apiClaro.crudApiClaro.resource;

import com.crud.apiClaro.crudApiClaro.model.Mobile;
import com.crud.apiClaro.crudApiClaro.repository.MobileRepository;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/claro")

public class MobileResource {



    @Autowired
    private MobileRepository mobileRepository;

    @PostMapping("/mobile")
    public ModelAndView addMobile(Mobile mobile){

        ModelAndView mv = new ModelAndView("redirect:/formCadastro.html");


        mobileRepository.save(mobile);
        
        if(mobile != null){
        	System.out.println("Cadastrou com Sucesso !!");
        }

        return mv;

    }

    @GetMapping("/mobile")
    public ModelAndView listarMobile(){
    	List<Mobile>listMobile = mobileRepository.findAll(); 
    	ModelAndView mv = new ModelAndView("redirect:/formCadastro.html");
    	mv.addObject("mobiles", listMobile);
    	return mv;
    }

    @GetMapping("/mobile/{id}")
    public ResponseEntity<Mobile> findByCode(@PathVariable Long id){
        Mobile mobile = mobileRepository.findOne(id);
        return mobile != null ? ResponseEntity.ok(mobile) : ResponseEntity.notFound().build();
    }

    @DeleteMapping("/mobile/{id}")
    public void removeMobile(@PathVariable Long id){
        mobileRepository.delete(id);
    }

    @PutMapping("/mobile{id}")
    public ResponseEntity<Mobile> updateMobile(@PathVariable Long id, @RequestBody Mobile mobile){
        Mobile mobilesalve = mobileRepository.findOne(id);
        BeanUtils.copyProperties(mobile, mobilesalve, "id");
        mobileRepository.save(mobilesalve);
        return ResponseEntity.ok(mobilesalve);
    }
}
