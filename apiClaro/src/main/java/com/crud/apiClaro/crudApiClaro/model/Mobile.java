package com.crud.apiClaro.crudApiClaro.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name= "mobile")
public class Mobile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "model" )
    private String model;

    @Column (name = "price" )
    private int price;

    @Column(name="brand")
    private String brand;
    
    @Column(name="photo")
    private String photo;

    @Column(name="date")
    private String date;

   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	 @Override
	    public boolean equals(Object o) {
	        if (this == o) return true;
	        if (o == null || getClass() != o.getClass()) return false;
	        Mobile mobile = (Mobile) o;
	        return Objects.equals(id, mobile.id);
	    }

}
