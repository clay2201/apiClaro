jQuery(document).ready(function(){
    $(".btnLimpar").click(function(){
        $("#razaoSocial").val("");
        $("#nomeFantasia").val("");
        $("#cep").val("");
        $("#logradouro").val("");
        $("#bairro").val("");
        $("#cidade").val("");
        $("#estado").val("");
        $("#telefone").val("");
        $("#email").val("");
    });

    $("#cep").bind("keyup blur focus", function(e) {
                e.preventDefault();
                var expre = /[^\d]/g;
                $(this).val($(this).val().replace(expre,''));
           });
    $("#telefone").bind("keyup blur focus", function(e) {
                    e.preventDefault();
                    var expre = /[^\d]/g;
                    $(this).val($(this).val().replace(expre,''));
               });
    $("#celular").bind("keyup blur focus", function(e) {
                    e.preventDefault();
                    var expre = /[^\d]/g;
                    $(this).val($(this).val().replace(expre,''));
               });

});